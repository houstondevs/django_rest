from django.contrib.auth import get_user_model
from django.utils.http import urlsafe_base64_decode
from django.utils.encoding import force_text

from rest_framework_simplejwt.views import TokenObtainPairView

from rest_framework import (
    viewsets,
    permissions,
    decorators,
    response,
    status,
    views,
    mixins,
    generics
)

from .serializers import (
    RegistrationSerializer,
    ProfileSerializer,
    PasswordResetSerializer,
    PasswordResetConfirmSerializer,
    MyTokenObtainPairSerializer,
    UserSerializer
)
from .models import Profile


from api.tasks import send_reset_message
from api.tokens import account_activation_token, password_reset_token
from api.permissions import IsObjectOwner


User = get_user_model()


class MyTokenObtainPairView(TokenObtainPairView):
    serializer_class = MyTokenObtainPairSerializer


@decorators.api_view(['POST', ])
@decorators.permission_classes((permissions.AllowAny,))
def registration_view(request):
    if request.method == 'POST':
        serializer = RegistrationSerializer(data=request.data)
        if serializer.is_valid():
            User.objects.create_user(
                email=serializer.data['email'],
                password=serializer.data['password']
            )
            data = {"email": serializer.data['email']}
            return response.Response({
                'message': 'Вы успешно зарегестрировались! Для активации аккаунта перейдите на почту! {}'.format(serializer.data['email']),
                'detail': data
                }
                , status=status.HTTP_201_CREATED
            )
        return response.Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@decorators.api_view(['GET', ])
@decorators.permission_classes((permissions.AllowAny,))
def activate(request, uidb64, token):
    if request.method == 'GET':
        try:
            uid = force_text(urlsafe_base64_decode(uidb64))
            user = User.objects.get(pk=uid)
        except:
            user = None
        if user is not None and account_activation_token.check_token(user, token):
            user.is_active = True
            user.save()
            return response.Response({'detail': 'Спасибо за подтверждение почты! Теперь вы можете войти!'})
        else:
            return response.Response({'detail': 'Ссылка на активацию больше не доступна!'})


class UserProfileView(generics.RetrieveUpdateAPIView):
    permission_classes = (permissions.IsAuthenticated, )
    serializer_class = ProfileSerializer

    def get_queryset(self):
        profile = Profile.objects.get(user=self.request.user)
        return profile

    def get_object(self):
        return self.get_queryset()


class PasswordResetView(views.APIView):
    permission_classes = (permissions.AllowAny, )

    def get_serializer(self, *args, **kwargs):
        return PasswordResetSerializer(*args, **kwargs)

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            user_email = serializer.data.get('email')
            if not User.objects.filter(email=user_email).exists():
                return response.Response({'detail': 'Пользователя с такой почтой не существует'}, status=status.HTTP_400_BAD_REQUEST)
            send_reset_message.delay(user_email, 'Ссылка на восстановление пароля!', 'messages/reset_password.html')
            return response.Response({'detail': 'Письмо для восстановления пароль отправленно на вашу почту!'}, status=status.HTTP_200_OK)
        return response.Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class PasswordResetConfirmView(views.APIView):
    permission_classes = (permissions.AllowAny, )

    def get_serializer(self, *args, **kwargs):
        return PasswordResetConfirmSerializer(*args, **kwargs)

    def get(self, request, uidb64, token):
        try:
            uid = force_text(urlsafe_base64_decode(uidb64))
            user = User.objects.get(pk=uid)
        except(TypeError, ValueError, OverflowError):
            user = None

        if user is not None and password_reset_token.check_token(user, token):
            return response.Response({'detail': 'Восстоновление пароля для {}'.format(user.email)})
        else:
            return response.Response({'detail': 'Ссылка восстоновление больше не доступна!'})

    def post(self, request, uidb64, token):
        serializer = self.get_serializer(data=request.data)
        try:
            uid = force_text(urlsafe_base64_decode(uidb64))
            user = User.objects.get(pk=uid)
        except(TypeError, ValueError, OverflowError):
            user = None

        if user is not None and password_reset_token.check_token(user, token):
            if serializer.is_valid():
                if user.check_password(serializer.data.get('new_password')):
                    return response.Response({'detail': 'Новый пароль не должен быть похож на старый!'}, status=status.HTTP_400_BAD_REQUEST)
                user.set_password(serializer.data.get('new_password'))
                user.save()
                return response.Response({'detail': 'Пароль изменен!'})
            return response.Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        return response.Response({'detail': 'Ссылка на восстоновление больше не доступна!'})


class UserViewSet(mixins.ListModelMixin,
                  mixins.RetrieveModelMixin,
                  viewsets.GenericViewSet):
    serializer_class = UserSerializer
    queryset = User.objects.all()
    permission_classes = (permissions.AllowAny,)


