# Generated by Django 2.2.6 on 2019-11-23 16:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app_accounts', '0007_auto_20191123_1617'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='birth_day',
            field=models.DateField(blank=True, null=True, verbose_name='дата рождения'),
        ),
    ]
