# Generated by Django 2.2.6 on 2019-11-23 15:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app_accounts', '0003_auto_20191121_1856'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='image',
            field=models.URLField(verbose_name='Аватарка'),
        ),
    ]
