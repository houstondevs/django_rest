from django.apps import AppConfig
from django.conf import settings


class AppAccountsConfig(AppConfig):
    name = 'app_accounts'

    def ready(self):
        if not settings.USER_IS_ACTIVE:
            import api.signals
        import app_accounts.signals