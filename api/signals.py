from django.dispatch import receiver
from django.db.models.signals import post_save
from django.contrib.auth import get_user_model


from api.tasks import send_activation_message

User = get_user_model()


@receiver(post_save, sender=User)
def activate_message(sender, instance, created, **kwargs):
    if created and instance.is_staff is not True:
        send_activation_message.delay(instance.email, 'Активация аккаунта!', 'messages/activate_account.html')



