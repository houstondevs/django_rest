from django.template.loader import render_to_string
from django.utils.http import urlsafe_base64_encode
from django.core.mail import EmailMessage
from django.utils.encoding import force_bytes
from django.contrib.auth import get_user_model

from .tokens import account_activation_token, password_reset_token

from celery import shared_task


User = get_user_model()


@shared_task
def send_activation_message(email, text, template):
    # Путь шаблона должен быть такой 'messages/activate_account.html'
    user = User.objects.get(email=email)
    current_site = 'localhost'
    mail_subject = '{} , {}'.format(current_site, text)
    message = render_to_string(template, {
        'user': user,
        'domain': current_site,
        'uid': urlsafe_base64_encode(force_bytes(user.pk)),
        'token': account_activation_token.make_token(user),
    })
    to_email = email
    email = EmailMessage(
        mail_subject, message, to=[to_email]
    )
    email.send()


@shared_task
def send_reset_message(email, text, template):
    # Путь шаблона должен быть такой 'messages/activate_account.html'
    user = User.objects.get(email=email)
    current_site = 'localhost'
    mail_subject = '{} , {}'.format(current_site, text)
    message = render_to_string(template, {
        'user': user,
        'domain': current_site,
        'uid': urlsafe_base64_encode(force_bytes(user.pk)),
        'token': password_reset_token.make_token(user),
    })
    to_email = email
    email = EmailMessage(
        mail_subject, message, to=[to_email]
    )
    email.send()
