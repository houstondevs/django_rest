from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.utils import six


class ActivationTokenGenerator(PasswordResetTokenGenerator):
    def _make_hash_value(self, user, timestamp):
        return (
            six.text_type(user.pk) + six.text_type(timestamp) +
            six.text_type(user.is_active)
        )


class PasswordResetGenerator(PasswordResetTokenGenerator):
    def _make_hash_value(self, user, timestamp):
        return (
            six.text_type(user.pk) + six.text_type(timestamp) +
            six.text_type(user.password)
        )


account_activation_token = ActivationTokenGenerator()
password_reset_token = PasswordResetGenerator()