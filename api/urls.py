from django.urls import path, include

from rest_framework import routers
from rest_framework_simplejwt.views import (
    TokenRefreshView
)

from app_accounts.views import (
    registration_view,
    activate,
    UserProfileView,
    PasswordResetView,
    PasswordResetConfirmView,
    MyTokenObtainPairView,
    UserViewSet
)
from Courses.views import CoursesView

router = routers.DefaultRouter()
router.register('users', UserViewSet)
router.register('courses', CoursesView)


urlpatterns = [
    path('api-auth/', include('rest_framework.urls')),
    path('', include(router.urls)),

    path('registration/', registration_view, name='registration'),
    path('activate/<uidb64>/<token>/', activate, name='activate'),
    path('token/', MyTokenObtainPairView.as_view(), name='token_obtain'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('me/profile/', UserProfileView.as_view(), name='profile_detail'),
    path('reset-password/', PasswordResetView.as_view(), name='password-reset'),
    path('reset-password/<uidb64>/<token>/', PasswordResetConfirmView.as_view(), name='password-reset-confirm'),

]
