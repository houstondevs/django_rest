from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import mixins, viewsets

from .models import Course
from .serializers import CoursesSerializer


class CoursesView(mixins.ListModelMixin,
                  mixins.RetrieveModelMixin,
                  viewsets.GenericViewSet):
    permission_classes = [IsAuthenticated,]
    serializer_class = CoursesSerializer
    queryset = Course.objects.all()
