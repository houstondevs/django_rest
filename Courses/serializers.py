from rest_framework import serializers
from .models import Course


class CoursesSerializer(serializers.ModelSerializer):
    img_title = serializers.ImageField()

    class Meta:
        model = Course
        fields = '__all__'
