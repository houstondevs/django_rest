from django.db import models
from cloudinary.models import CloudinaryField


class Course(models.Model):
    class Meta:
        verbose_name = "Курс"
        verbose_name_plural = "Курсы"

    title = models.CharField(max_length=10, verbose_name="Заголовок")
    img_title = CloudinaryField()
    text = models.TextField()